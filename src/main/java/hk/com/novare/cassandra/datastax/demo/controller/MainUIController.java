package hk.com.novare.cassandra.datastax.demo.controller;

import hk.com.novare.cassandra.datastax.demo.dao.CassandraDao;
import hk.com.novare.cassandra.datastax.demo.dao.Crud;
import hk.com.novare.cassandra.datastax.demo.dao.MySQLDao;
import hk.com.novare.cassandra.datastax.demo.model.Sili;
import hk.com.novare.cassandra.datastax.demo.services.SiliService;
import hk.com.novare.cassandra.datastax.demo.util.DatastaxUtil;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;

import java.net.URL;
import java.util.ResourceBundle;

public class MainUIController implements Initializable {

    @FXML private TableView<Sili> tblView;
    @FXML private TableColumn<Sili, String> colId, colName, colSpicinessLevel;
    @FXML private ToggleButton tglCassandra, tglMysql;
    @FXML private VBox leftPane;
    @FXML private BorderPane contentPane;
    @FXML private TextField txtId, txtName, txtLevel;
    @FXML private Label lblForm;

    private SiliService<Sili> service;
    private MySQLDao mysqlProvider;
    private CassandraDao cassandraProvider;

    private Sili itemToEdit = null;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        service = new SiliService<>();
        mysqlProvider = new MySQLDao();
        cassandraProvider = new CassandraDao();

        colId.setCellValueFactory(new PropertyValueFactory<>("id"));
        colName.setCellValueFactory(new PropertyValueFactory<>("name"));
        colSpicinessLevel.setCellValueFactory(new PropertyValueFactory<>("spicinessLevel"));
        tblView.itemsProperty().bind(service.valueProperty());

        setDbProvider();

        contentPane.setLeft(null);
    }

    private void setDbProvider() {
        if (tglCassandra.isSelected())
            service.setCrud(cassandraProvider);
        else if (tglMysql.isSelected())
            service.setCrud(mysqlProvider);

        service.operate();
    }

    private void clearForm() {
        txtId.clear();
        txtLevel.clear();
        txtName.clear();
    }

    private void setLeftPaneVisible(boolean val, String caption) {
        if (val) {
            lblForm.setText(caption);
            contentPane.setLeft(leftPane);
        } else
            contentPane.setLeft(null);
    }

    @FXML private void showAddForm(ActionEvent evt) {
        setLeftPaneVisible(true, "ADD ENTRY");
        txtId.setText(DatastaxUtil.generateId());

        itemToEdit = null;
    }

    @FXML private void showEditForm(ActionEvent evt) {
        itemToEdit = tblView.getSelectionModel().getSelectedItem();

        if (itemToEdit != null) {
            setLeftPaneVisible(true, "EDIT ENTRY");

            txtId.setText(itemToEdit.getId());
            txtName.setText(itemToEdit.getName());
            txtLevel.setText(itemToEdit.getSpicinessLevel());

        }
    }

    @FXML private void setDBProvider(ActionEvent evt) {
        setDbProvider();
    }

    @FXML private void deleteItem(ActionEvent evt) {
        Sili selectedSili = tblView.getSelectionModel().getSelectedItem();

        if (selectedSili != null) {
            service.delete(selectedSili);
        }
    }

    @FXML private void closeForm(ActionEvent evt) {
        contentPane.setLeft(null);
    }

    @FXML private void saveForm(ActionEvent evt) {
        if (itemToEdit == null) {
            Sili sili = new Sili();
            sili.setId(txtId.getText());
            sili.setName(txtName.getText());
            sili.setSpicinessLevel(txtLevel.getText());

            service.create(sili);
        } else {
            itemToEdit.setId(txtId.getText());
            itemToEdit.setName(txtName.getText());
            itemToEdit.setSpicinessLevel(txtLevel.getText());

            service.update(itemToEdit);
            itemToEdit = null;
        }

        setLeftPaneVisible(false, "");
        clearForm();

    }
}
