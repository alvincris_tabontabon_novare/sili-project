package hk.com.novare.cassandra.datastax.demo.dao;


import hk.com.novare.cassandra.datastax.demo.model.Sili;
import hk.com.novare.cassandra.datastax.demo.util.MysqlUtil;
import lombok.Cleanup;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MySQLDao implements Crud<Sili> {

    @Override
    public void create(Sili item) throws SQLException {
        try {
            @Cleanup Connection connection = MysqlUtil.getConnection(MysqlUtil.getDatabase());
            @Cleanup PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO sili(id,name,spiciness) VALUES(?, ?,?)");
            preparedStatement.setString(1, item.getId());
            preparedStatement.setString(2, item.getName());
            preparedStatement.setString(3, item.getSpicinessLevel());
            preparedStatement.execute();
        } catch (SQLException e) {
            System.err.println(e.getLocalizedMessage());
        }
    }

    @Override
    public void update(Sili item) {
        try {
            @Cleanup Connection connection = MysqlUtil.getConnection(MysqlUtil.getDatabase());
            @Cleanup PreparedStatement preparedStatement = connection.prepareStatement("UPDATE sili SET name=?, spiciness=? WHERE id=?");
            preparedStatement.setString(1, item.getName());
            preparedStatement.setString(2, item.getSpicinessLevel());
            preparedStatement.setString(3, item.getId());
            preparedStatement.execute();
        } catch (SQLException e) {
            System.err.println(e.getLocalizedMessage());
        }
    }

    @Override
    public void delete(Sili item) {
        try {
            @Cleanup Connection connection = MysqlUtil.getConnection(MysqlUtil.getDatabase());
            @Cleanup PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM sili WHERE id=?");
            preparedStatement.setString(1, item.getId());
            preparedStatement.execute();
        } catch (SQLException e) {
            System.err.println(e.getLocalizedMessage());
        }
    }

    @Override
    public List<Sili> readAll() {
        String query = "SELECT * FROM sili";

        List<Sili> mgaSili = new ArrayList<>();
        try {
            @Cleanup ResultSet resultSet = MysqlUtil.getResultSet(query);
            while (resultSet.next()){
                mgaSili.add(new Sili(resultSet.getString("id"), resultSet.getString("name"), resultSet.getString("spiciness")));
            }
        } catch (SQLException e) {
            System.err.println(e.getLocalizedMessage());
        }
        return mgaSili;
    }
}
