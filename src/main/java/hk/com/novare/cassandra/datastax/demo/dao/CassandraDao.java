package hk.com.novare.cassandra.datastax.demo.dao;


import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Session;
import hk.com.novare.cassandra.datastax.demo.model.Sili;
import hk.com.novare.cassandra.datastax.demo.util.DatastaxUtil;
import lombok.Cleanup;

import java.util.ArrayList;
import java.util.List;

public class CassandraDao implements Crud<Sili> {

    @Override
    public void create(Sili item) {
        @Cleanup Session session = DatastaxUtil.getCluster().connect();
        PreparedStatement statement = session.prepare("INSERT INTO silikeyspace.sili" + "(id, name, spiciness)"
                        + "VALUES (?,?,?);");

        BoundStatement boundStatement = new BoundStatement(statement);
        session.execute(boundStatement.bind(item.getId(), item.getName(), item.getSpicinessLevel()));
    }

    @Override
    public void update(Sili item) {
        @Cleanup Session session = DatastaxUtil.getCluster().connect();
        PreparedStatement statement = session.prepare(
                "UPDATE silikeyspace.sili set name = ?, spiciness  = ? where id = ?");

        BoundStatement boundStatement = new BoundStatement(statement);
        session.execute(boundStatement.bind(item.getName(), item.getSpicinessLevel(), item.getId()));
    }

    @Override
    public void delete(Sili item) {
        @Cleanup Session session = DatastaxUtil.getCluster().connect();
        PreparedStatement statement = session.prepare(
                "DELETE FROM silikeyspace.sili where id = ?");

        BoundStatement boundStatement = new BoundStatement(statement);
        session.execute(boundStatement.bind(item.getId()));
    }

    @Override
    public List<Sili> readAll() {
        @Cleanup Session session = DatastaxUtil.getCluster().connect();

        List<Sili> lists = new ArrayList<>();
        ResultSet results = session.execute("SELECT * FROM silikeyspace.sili");
        results.all().stream().forEach(row -> {
            Sili sili = new Sili();
            sili.setId(row.getString("id"));
            sili.setName(row.getString("name"));
            sili.setSpicinessLevel(row.getString("spiciness"));

            lists.add(sili);
        });

        return lists;
    }
}
