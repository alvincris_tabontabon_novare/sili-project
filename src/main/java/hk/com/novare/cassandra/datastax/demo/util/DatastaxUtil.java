package hk.com.novare.cassandra.datastax.demo.util;

import com.datastax.driver.core.*;
import com.datastax.driver.core.querybuilder.QueryBuilder;
import lombok.Cleanup;
import lombok.Getter;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;


public final class DatastaxUtil {

    @Getter
    private static Cluster cluster;

    public static void createSchema(String keyspaceName, String classStrategy, String replicationFactor) {
        if (cluster == null)
            throw new NullPointerException("No cluster instance");


        String query = String.format("CREATE KEYSPACE %s WITH replication = {'class':'%s', 'replication_factor':%s};",
                                    keyspaceName, classStrategy, replicationFactor);

        @Cleanup Session session = cluster.connect();
        session.execute(query);
    }

    public static String generateId() {
        return String.format("%010d", ThreadLocalRandom.current().nextInt(1000));
    }

    public static void createSchema(String keyspaceName) {
        createSchema(keyspaceName, "SimpleStrategy", "1");
    }

    /**
     *
     * @param tableName
     */
    public static void createTable(String tableName) {
        String query = String.format("CREATE TABLE %s (id text PRIMARY KEY, name text, spiciness text)", tableName);

        @Cleanup Session session = cluster.connect();
        session.execute(query);
    }

    public static void connect(String node) {
        cluster = Cluster.builder()
                .addContactPoint(node)
                .build();
    }

    public static void close() {
        cluster.close();
    }


    public static Session session(){
        @Cleanup Session session = cluster.connect();
        return session;
    }

    public static List<Row> listOfSili( String table) {
        Statement statement = QueryBuilder
                .select()
                .all()
                .from(table);
        return session().execute(statement).all();
    }
}
