package hk.com.novare.cassandra.datastax.demo;


import hk.com.novare.cassandra.datastax.demo.util.DatastaxUtil;
import hk.com.novare.cassandra.datastax.demo.util.MysqlUtil;
import hk.com.novare.cassandra.datastax.demo.util.Views;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class MainApp extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource(Views.MAIN.resource));

        Stage stage = new Stage();
        stage.setOnCloseRequest(event -> {
            DatastaxUtil.close();
            System.exit(0);
        });

        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        try {
            DatastaxUtil.connect("127.0.0.1");
            DatastaxUtil.createSchema("silikeyspace");
            DatastaxUtil.createTable("silikeyspace.sili");
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        }

        try {
            MysqlUtil.createDB("silikeyspace");
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        }

        launch(args);
    }
}
