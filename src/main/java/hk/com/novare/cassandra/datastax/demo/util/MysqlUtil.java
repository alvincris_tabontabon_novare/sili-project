package hk.com.novare.cassandra.datastax.demo.util;

import lombok.Getter;
import lombok.Setter;

import java.io.StringReader;
import java.sql.*;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by bujawe99 on 7/24/15.
 */
public final class MysqlUtil {

    private static Connection connection = null;
    private static Statement statement = null;
    private static ResultSet resultSet = null;
    private static String Url = "jdbc:mysql://localhost:3306/";
    private static String DBUsername = "root";
    private static String DBPassword = "ShsY148";
    @Setter @Getter
    private static String database;

    private MysqlUtil() {

    }

    public static void createDB(String dbName) throws SQLException {
        MysqlUtil.setDatabase(dbName);
        connection = MysqlUtil.getConnection();
        String  createDB = "CREATE DATABASE " + dbName;
        String createTable = "CREATE TABLE sili (id varchar(255) PRIMARY KEY NOT NULL, name varchar(255), spiciness varchar(255))";

        MysqlUtil.getStatement().execute(createDB);
        connection.close();

        connection = MysqlUtil.getConnection(dbName);

        MysqlUtil.getStatement().execute(createTable);
        connection.close();

    }

    public static Connection getConnection()
    {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(Url, DBUsername, DBPassword);
        } catch (ClassNotFoundException e) {
            System.err.println(e.getLocalizedMessage());
        } catch (SQLException e) {
            System.err.println(e.getLocalizedMessage());
        }
        return connection;
    }

    public static Connection getConnection(String databaseName) {
        String DBUrl = Url + databaseName;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(DBUrl, DBUsername, DBPassword);
        } catch (ClassNotFoundException e) {
            System.err.println(e.getLocalizedMessage());
        } catch (SQLException e) {
            System.err.println(e.getLocalizedMessage());
        }
        return connection;
    }

    public static Statement getStatement() {
        try {
            statement = MysqlUtil.getConnection(getDatabase()).createStatement();
        } catch (SQLException e) {
            System.err.println(e.getLocalizedMessage());
        }
        return statement;
    }

    public static ResultSet getResultSet(String sql) {
        try {
            resultSet =  MysqlUtil.getStatement().executeQuery(sql);
        } catch (SQLException e) {
            System.err.println(e.getLocalizedMessage());
        }
        return resultSet;
    }

    public static String generateId() {
        return String.format("%010d", ThreadLocalRandom.current().nextInt(1000));
    }
}
