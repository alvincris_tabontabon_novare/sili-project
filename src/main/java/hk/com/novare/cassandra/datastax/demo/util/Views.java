package hk.com.novare.cassandra.datastax.demo.util;


public enum Views {

    MAIN("main", "/views/MainUI.fxml");

    public String resource;
    public String name;

    Views(String name, String resource) {
        this.resource = resource;
        this.name = name;
    }
}
