package hk.com.novare.cassandra.datastax.demo.services;

import hk.com.novare.cassandra.datastax.demo.dao.Crud;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import lombok.Setter;

import java.sql.SQLException;
import java.util.Collection;


public class SiliService<T> extends Service<ObservableList<T>> {

    private boolean started = false;

    @Setter
    private Crud<T> crud;

    @Override
    protected Task<ObservableList<T>> createTask() {
        return new SiliTask<>();
    }

    class SiliTask<T> extends Task<ObservableList<T>> {

        @Override
        protected ObservableList<T> call() throws Exception {
            return FXCollections.observableArrayList((Collection<? extends T>) crud.readAll());
        }
    }

    public void create(T item) {
        try {
            crud.create(item);
            operate();
        } catch (SQLException e) {
            System.err.println(e.getLocalizedMessage());
        }
    }

    public void update(T item) {
        try {
            crud.update(item);
            operate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void delete(T item) {
        crud.delete(item);
        operate();
    }

    public void operate() {
        if (!started) {
            start();
            started = true;
        } else {
            restart();
        }
    }
}
