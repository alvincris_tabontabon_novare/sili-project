package hk.com.novare.cassandra.datastax.demo.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Sili {

    private String id;
    private String name;
    private String spicinessLevel;

    public Sili(String id, String name, String spiciness) {
        this.id = id;
        this.name = name;
        this.spicinessLevel = spiciness;
    }
}
