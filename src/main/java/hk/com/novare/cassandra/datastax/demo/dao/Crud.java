package hk.com.novare.cassandra.datastax.demo.dao;

import java.sql.SQLException;
import java.util.List;


public interface Crud<T> {

    void create(T item) throws SQLException;
    void update(T item) throws SQLException;
    void delete(T item);
    List<T> readAll();

}
